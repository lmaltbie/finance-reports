# Finance Reports

This tool was written to provide digestible financial reports for the author. It is built upon the freports (finance reports) tool. The main item of this tool is the report object. 

## freports Overview

The report object is treated as a standalone instance of the reports listed below. The object contains a connection to its own database and all information stored in it. 

The author considered attaching a pandas dataframe to the report object to make the data more easily accessible to the user, however it was deliberately decided to *not* do this in its current form. The reasoning for this was to avoid creating two sources of information, as the pandas dataframe would be accessible to the user, whereas the SQLite database is meant to contain the information. 

Future iterations of this tool may revisit this design, however at this time this is not acessible. 


## Trending Stock Performance

Trending performance includes calculating the following:

1. Closing price of item (close)
2. Simple moving average over the course of 10 days (sma1)
3. Simple moving average over the course of 25 days (sma2)
4. Natural log returns over course of the day (logreturn)

## Database

### Databases 
This SQLite database contains the foundational information for use in the Trending Stock Performance calculations.

The table named "trending" is the main table. Its columns are as follows:
- id INTEGER PRIMARY KEY AUTOINCREMENT
- symbol STRING NOT NULL
- date REAL NOT NULL
- close FLOAT
- sma1 FLOAT
- sma2 FLOAT
- logreturn FLOAT

## Reports

Coming soon....
