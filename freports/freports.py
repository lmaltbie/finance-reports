# %%
import yfinance as yf
import pandas as pd
import numpy as np
import datetime
from sql_tools import sqlite_client

class report:
    def __init__(self,report_name, stocks, table_name, start_date = '2021-01-01'):
        self.name = report_name # name of the report
        self.stocks = stocks # list of tickers to use
        self.start_date = start_date # string type
        self.db_name = self.name + '_db' 
        self.table_name = table_name

        path = input("Filepath to database for %s's data (do not include name): " % self.table_name)
        full_path = path + "/" + self.db_name + '.sqlite'
        self.db_path = full_path

        try:
            self.db = sqlite_client(self.db_path)
            last_date = self.get_latest_date()
            print('Database already found. Consider updating.')
            print('Last updated on %s' % last_date)
        except Exception as e:
            print(e)
            print('Creating database....')
            self.__create_db()

    def __create_db(self):
        tickers = self.stocks # easier by convention for yf
        today = str(datetime.date.today())
        initial_data = yf.download(tickers = tickers,
                                   start = self.start_date, 
                                   end = today, 
                                   interval = '1d'
        )
        # establish name of database and location to store it


        # intialize database object and attach it to this report object
        self.db = sqlite_client(self.db_path)

        # create table
        create_db_query =  '''
        CREATE TABLE %s (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            symbol STRING NOT NULL,
            date REAL NOT NULL,
            close FLOAT,
            sma1 FLOAT,
            sma2 FLOAT,
            logreturn FLOAT
        )
        ''' % self.table_name

        self.db.execute_query(create_db_query)

        # Needs to change as calculations are changed
        self.calcs = ['close','sma1','sma2','logreturn'] 

        # create database to keep for now
        cols = pd.MultiIndex.from_product([tickers,self.calcs],names=('stocks','calcs'))
        calc_data = pd.DataFrame(index=initial_data.index,columns=cols)

        for stock in tickers: # still hard coding calculations, need to determine how to change that
            # close
            calc_data[stock,'close'] = initial_data['Close',stock]

            # sma1 (10 days)
            calc_data[stock,'sma1'] = initial_data['Close',stock].rolling(10).mean()

            # sma2 (25 days)
            calc_data[stock,'sma2'] = initial_data['Close',stock].rolling(25).mean()

            # log return
            calc_data[stock,'logreturn'] = np.log(initial_data['Close',stock]/initial_data['Close',stock].shift(1))
        
        # fill NaN with 0's
        calc_data = calc_data.fillna(0)

        # create base statement to use
        insert_statement_base = '''INSERT INTO 
        %s (symbol, date, close, sma1, sma2, logreturn)
        VALUES\n''' % self.table_name
        insert_statement = insert_statement_base
        for stock in tickers:
            for i in calc_data.index:
                # easier to read
                date_val = i.to_julian_date()
                close_val = calc_data.loc[i][stock,'close']
                sma1_val = calc_data.loc[i][stock,'sma1']
                sma2_val = calc_data.loc[i][stock,'sma2']
                logreturn_val = calc_data.loc[i][stock,'logreturn']

                # create a new statement to add
                insert_statement_addition = '("%s", %s, %s, %s, %s, %s),\n' % \
                                        (stock, date_val, close_val, sma1_val, sma2_val, logreturn_val)
                insert_statement += insert_statement_addition

        # need to terminate SQL statement properly or SQLite gets mad, drop the new line
        insert_statement = insert_statement[:-2] + ';'
        self.insert_statement = insert_statement

        # push values to database
        self.db.execute_query(insert_statement)
    
    def get_latest_date(self):
        '''
        Retrieves last date that the database pulled data. 
        Assumes date column is julian date. 
        Parameters:
        self.table_name = needs to know table name
        self.connection = connection to database. Will verify database exists
        Returns:
        latest_date: Most recent date of the table
        '''

        # read query gets the latest date 
        date_query = '''SELECT datetime(date) as d from %s
        ORDER BY d desc
        LIMIT 1
        ''' % self.table_name
        
        # execute read query, only really care about the value in the tuple 
        last_date = self.db.execute_read_query(date_query)[0][0]
        
        return last_date
    
    def get_latest_data(self):
        '''
        Retrieves latest data from database table for 44 days from last_date.  

        Parameters:
        connection: sqlite connection object to database containing table
        table_name: string type containing name of table
        last_date: datetime item, last date in the table, a la the last time the database was updated

        Returns:
        history_data: list of tuples with data values
        '''

        last_date = self.get_latest_date()
        last_date = datetime.datetime.strptime(last_date,'%Y-%m-%d %H:%M:%S')
        # need to look farther back in time to get moving average calcs. Need 44 days to account for weekends and holidays. 
        needed_date = last_date - datetime.timedelta(days=44)
        # change the format to be nice
        needed_date = needed_date.strftime('%Y-%m-%d')
        # initialize query
        collect_data_query = 'SELECT symbol, datetime(date) as d, '
        
        # add to query based on list of calculated values
        for c in self.calcs:
            collect_data_query = collect_data_query + c + ', '
        
        collect_data_query = collect_data_query[:-2]

        collect_data_query  += '''
        FROM %s
        WHERE d >= "%s"
        ORDER BY d ASC
        ''' % (self.table_name,str(needed_date))

        history_data = self.db.execute_read_query(collect_data_query)

        return history_data
    
    def make_data_frame(self,sql_data_list,values_names):
        '''
        Function that takes data from SQL query and returns a pandas dataframe. 
        Assumes the following:
        1) SQL query data is a list of tuples
        2) Values are provided for very tuple, i.e. tuples are all the same size. 

        Parameters:
        sql_data_list: list of tuples containing all data required to be placed into Pandas dataframe
                      Note: tuples must contain at the least the following info, in order listed:
                      - date: string of datetime format, will be made the index, format '%Y-%m-%d %H:%M:%S'
                      - symbol: name of stock, will be used as first layer of multi index
                      - value parameters: can be multiple parameters (as detailed below)
        values_names: list containing names of value parameters provided in sql_data_list, after the date and
                      symbol names

        Returns:
        data = Pandas dataframe containing the data. 
        '''

        calcs = self.calcs

         # initialize some variables, check to make sure the right data was provdied
        if len(sql_data_list[0]) < 2:
            raise Exception('Data provided does not contain enough information to make dataframe.')
        else:
            date_list = []
            stock_list = []
            data_list = []

        for d in sql_data_list: # iterate through all rows in the list of all data to parse
            if d[0] in stock_list: # if this is a unique stock, add it to the list of stocks otherwise pass
                pass
            else:
                stock_list.append(d[0])

            if d[1] in date_list: # if this is a unique date, add it to list of dates otherwise pass
                pass
            else:
                date_list.append(d[1]) # append 


            data_list.append(d[2:len(d)]) # add the real data to list of data

        # Make your multi index and then your dataframe
        cols = pd.MultiIndex.from_product([stock_list,values_names],names=('stocks','values'))
        data = pd.DataFrame(index = pd.DatetimeIndex(date_list),columns = cols) # want datetimes
        format = '%Y-%m-%d %H:%M:%S'

        for d in sql_data_list:
            n = 2 # need to start at 3rd item in tuple to append 
            stock = d[0]
            date_str = d[1]
            # date = datetime.strptime(date_str,format)

            for v in values_names:
                data.loc[date_str][stock,v.lower()] = d[n]
                n+=1

        return data

    def delete_date_data(self): 
        '''
        Deletes an entire date worth of data from the database

        Parameters: 
        date_to_delete: datetime object of date that you want to delete 

        Returns:
        success: boolean indicating deletion was successful
        '''

    
    def update_db(self):
        last_date = self.get_latest_date() # get latest date in database
        last_date_obj = datetime.datetime.strptime(last_date,'%Y-%m-%d %H:%M:%S') # convert to datetime object

        latest_data = self.get_latest_data() # retrieve data 
        old_data = self.make_data_frame(latest_data,self.calcs) # make it a dataframe
        
        today = datetime.datetime.today() # get today's date
        end_day = today + datetime.time(days=1) # yfinance is non-inclusive on end-date
        end_day_str = datetime.strftime(end_day,'%Y-%m-%d') # need string for yfinance
        
        stonks = old_data.columns.unique(level = 'stocks').to_list() # get unique values of stocks in database

        new_data = yf.download(
                                tickers = stonks,
                                start = last_date,
                                end = end_day_str,
                                interval = '1d'
        ) 

        # need to separate and then drop last date, since it's already included in the database
        # so needs to be treated separately (update that record instead of appending)
        if new_data.iloc[0].name == last_date:
            # remove it to separate dataframe, then do calcs
            last_record_date_data = new_data.loc[last_date]

            last_record_cols = pd.MultiIndex.from_product([stonks,self.calcs],names = ('stocks','calcs'))
            last_record_calcs = pd.DataFrame(index=last_date,columns=last_record_cols)

            for stock in stonks:
                # close
                last_record_calcs.loc[last_date][stock,'close'] = new_data.loc[last_date]['Close',stock]

                # sma1 (10 days)
                last_record_calcs.loc[last_date][stock,'sma1'] = new_data[stock,'close'].rolling(10).mean()[str(last_date)]

                # sma2 (25 days)
                last_record_calcs.loc[last_date][stock,'sma2'] = new_data[stock, 'close'].rolling(25).mean()[str(last_date)]

                # log return
                last_record_calcs.loc[last_date][stock,'logreturn'] = \
                    np.log(last_record_calcs.loc[last_date][stock,'close']/last_record_calcs[stock, 'close'].shift(1)[str(last_date)])

            # make an update statement
            update_statement = '''
                UPDATE
                    %s
                SET 
                ''' % (self.table_name)
            for stock in stonks:
                for calc in self.calcs:
                    update_statement += '%s = %s,\n' % (calc,last_record_calcs.loc[last_date][stock,calc])
            
            update_statement+='''
                WHERE
                    date = %s
            ''' % (last_date.to_julian_date())

            new_data = new_data.drop(last_date)

        new_index = old_data.index.append(new_data.index)
        calc_data = old_data.copy(deep=True)

        for d in new_data.index:
            calc_data.loc[d] = 0
            for stock in stonks: # iterate through every stock 
                '''
                A little bit of manual calculations - just simply goes through every known calculation
                that needs to be ran and just does them. For initial commit to the SQL table, brute
                force was deemed ok. 
                '''
                
                # close
                calc_data.loc[d][stock,'close'] = new_data.loc[d]['Close',stock]

                # sma1 (10 days)
                calc_data.loc[d][stock,'sma1'] = calc_data[stock,'close'].rolling(10).mean()[str(d)]

                # sma2 (25 days)
                calc_data.loc[d][stock,'sma2'] = calc_data[stock, 'close'].rolling(25).mean()[str(d)]

                # log return
                calc_data.loc[d][stock,'logreturn'] = \
                    np.log(calc_data.loc[d][stock,'close']/calc_data[stock, 'close'].shift(1)[str(d)])
        
        calc_data = calc_data.fillna(0)        

        insert_statement = '''INSERT INTO
        %s (symbol, date, close, sma1, sma2, logreturn)
        VALUES\n''' % self.table_name

        for stock in stonks:
            for d in new_data.index:
                date_val = d.to_julian_date()
                close_val = calc_data.loc[d][stock,'close']
                sma1_val = calc_data.loc[d][stock,'sma1']
                sma2_val = calc_data.loc[d][stock,'sma2']
                logreturn_val = calc_data.loc[d][stock,'logreturn']

                # insert into statement
                insert_statement += '("%s", %s, %s, %s, %s, %s),\n' \
                                    % (stock,date_val,close_val,sma1_val,sma2_val,logreturn_val)
        
        insert_statement = insert_statement[:-2] + ';'

        return (insert_statement,update_statement)

if __name__ == '__main__': 
    stocks = ['AAPL','VOO','VTI','TSLA']
    table_name = 'test_data'
    foo = report(report_name ='test_report',stocks = stocks,table_name = table_name)
    # last_date = foo.get_latest_date()
    # data = foo.get_latest_data()
