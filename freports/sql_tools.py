import sqlite3
from sqlite3 import Error

class sqlite_client:
    '''
    Object meant to attach to a sqlite database. 

    Initializing requires only providing a valid path to the database

    '''
    def __init__(self, db_path):
        self.path = db_path
        self.create_connection()

    def create_connection(self):
        '''
        Function to generate connection to database as object item (self.connection)

        Inputs:
            Path: path to SQLite database
        
        '''
        connection = None
        print("Trying to connect to: %s" % self.path)
        try:
            connection = sqlite3.connect(self.path)
            print("Connection to SQLite DB successful")
            print("Connected to %s" % self.path)

        except Error as e:
            print(f"The error '{e}' occurred")

        self.connection = connection

    def execute_query(self, query):
        '''
        Executes SQLite query

        Inputs:
            connection: SQLite connection object
            query: string containing query to be executed
        '''

        cursor = self.connection.cursor()
        try:
            cursor.execute(query)
            self.connection.commit()
            print("Executed query successfully")
        except Error as e:
            print(f"The error '{e}' occurred")

    def execute_read_query(self, query):
        cursor = self.connection.cursor()
        result = None
        try:
            cursor.execute(query)
            result = cursor.fetchall()
            return result
        except Error as e:
            print(f"The error '{e}' occurred")