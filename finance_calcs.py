#%% 
import yfinance as yf
import pandas as pd
import numpy as np
import datetime
import sqlite3
from sqlite3 import Error


class stock():
    '''
    Base stock object used every time this script is ran
    '''
    def __init__(self,symb):
        self.symb = symb # make ticker/symbol an attriute 
    
    def get_prices(self,start_date, end_date, int = '1m'): # get prices for specified span, default 1minute spacing
        '''
        Retrieves the prices using yfinance using start and end dates.

        Parameters:
        int (string): 
            can be any of the following:
            data interval (1m data is only for available for last 7 days, and data interval <1d for the last 60 days) 
            Valid intervals are:
            “1m”, “2m”, “5m”, “15m”, “30m”, “60m”, “90m”, “1h”, “1d”, “5d”, “1wk”, “1mo”, “3mo”
            Defaults to '1m'
        
        Generates a pandas dataframe named self.prices
        '''
        
        datas = yf.download(
            tickers = self.symb,
            start = start_date,
            end = end_date,
            interval = int
        )
        self.prices = datas 
    
    def daily_calculations(self,length):
        '''
        Performs calculations for day prices. 

        Parameters:
        length (int) :
            Defines the length of the rolling average in data points
            Note that this is likely a function of the interval used to get prices.
        
        Generates a pandas dataframe named self.calcs 

        '''
        try: # check if you got the price data already, if not go get data 
            self.calcs = pd.DataFrame(index=self.prices.index)

            # Rolling average with length defined as a passed data 
            self.calcs['Rolling Average']=self.prices['Close'].rolling(length).mean()

            # Logrithmic returns
            self.calcs['Log Returns']=np.log(self.prices['Close']/self.prices['Close'].shift(1))


        except AttributeError: 
            print("Error: Can't do calculations until you get prices")
            print("Try using self.get_prices first!")
    
def make_trending_values(stock,connection):
        '''
        Generates the trending values to be used in trending database.
        
        Outputs:
            Trends: dictionary keyed with the parameters below

        Parameters:
            symbol = ticker, normally written as self.symb
            date = julian date 
            close = closing value of stock for that day 
            sma1 = 10 day moving average
            sma2 = 25 day moving average
            logreturn = log (base e) return for the day
        '''
        symbol = stock.symb # ticker name
        date = stock.prices.index[-1].to_julian_date() # julian date of last timestamp
        close = stock.prices['Close'][-1] # last value of price
        
        # sma1 calculation
        # make a load values of 'close' from db, sort by date and grab last 24 values
        # take average of these last 9 values plus the newest close value (need to filter)

        sma1_collect_query = '''
                            SELECT sma1 from finance
                            where symbol = %s
                            ''' % symbol
        
        sma1 = execute_read_query(connection, sma1_collect_query)

        # sma2 calculation
        # take average of last 24 values plus the last value of 'close' to assign to sma2

        # logreturn calculation
        # grab the last close value and then take log(close_today/close_last)
    
def create_connection(path):
    '''
    Function to generate connection to database

    Inputs:
        Path: path to SQLite database
    
    Outputs:
        connection: SQLite database connection object
    '''
    connection = None
    print("Trying to connect to: %s" % path)
    try:
        connection = sqlite3.connect(path)
        print("Connection to SQLite DB succespipsful")
        print("Connected to %s" % path)
    except Error as e:
        print(f"The error '{e}' occurred")

    return connection

def execute_query(connection, query):
    '''
    Executes SQLite query

    Inputs:
        connection: SQLite connection object
        query: string containing query to be executed
    '''

    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        print("Executed query successfully")
    except Error as e:
        print(f"The error '{e}' occurred")

def execute_read_query(connection, query):
    cursor = connection.cursor()
    result = None
    try:
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    except Error as e:
        print(f"The error '{e}' occurred")

if __name__ == '__main__':
    tickers = 'VOO'
    start = '2021-10-22'
    end = '2021-10-23'

    voo = stock(tickers)
    voo.get_prices(start_date = start,end_date = end)
    voo.daily_calculations(length=10)
